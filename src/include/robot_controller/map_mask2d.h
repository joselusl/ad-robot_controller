
#ifndef _MAP_MASK2D_H_
#define _MAP_MASK2D_H_


// vector
#include <vector>

// string
#include <string>

// thread
#include <thread>

// Eigen
#include <Eigen/Dense>






///////////////////////////////////////////
/// \brief The MapMask class
///////////////////////////////////////////
class Map2DMask
{
public:
    int width_; // cells
    int height_; // cells

public:
    double resolution_; // m / cell

protected:
    Eigen::MatrixXi mask_; // cell x cell
public:
    void prepareMask();
    void initMaskToOnes();
public:
    Eigen::MatrixXi getMask() const;
    int getMaskValue(int pos_x, int pos_y) const;
    void setMaskValue(int value, int pos_x, int pos_y);

public:
    bool isOk() const;

public:
    int getValidArea() const;

};







#endif
