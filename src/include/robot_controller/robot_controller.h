
#ifndef _ROBOT_CONTROLLER_H_
#define _ROBOT_CONTROLLER_H_


// vector
#include <vector>

// string
#include <string>

// thread
#include <thread>

// Eigen
#include <Eigen/Dense>

// ros
#include <ros/ros.h>

//
#include <std_srvs/Empty.h>

//
#include <geometry_msgs/PoseStamped.h>

//
#include <nav_msgs/Path.h>
#include <nav_msgs/OccupancyGrid.h>

// tf
#include <tf/transform_datatypes.h>
#include <tf/transform_listener.h>
//#include <tf/transform_broadcaster.h>

// action
#include <actionlib/client/simple_action_client.h>
#include <actionlib/server/simple_action_server.h>

// move_base_msgs
#include <move_base_msgs/MoveBaseAction.h>
#include <move_base_msgs/MoveBaseActionGoal.h>


// exploration_msgs
#include <exploration_msgs/ExplorationMission.h>


// map mask 2d
#include "robot_controller/map_mask2d.h"

// mission planner
#include "robot_controller/mission_planner.h"



////////////////////////////////
/// \brief The RobotState enum
//////////////////////////////////
enum class RobotState
{
    stopped,
    started,
};






////////////////////////////
/// \brief The RobotController class
/////////////////////////////
class RobotController
{
public:
    RobotController(int argc,char **argv);
    ~RobotController();

    // Node handler
protected:
    ros::NodeHandle* nh_;

    // Tf
protected:
    tf::TransformListener* tf_transform_listener_;
    // tf::TransformBroadcaster* tf_transform_broadcaster_;

    // frames names
protected:
    std::string robot_frame_name_;
    std::string world_frame_name_;

    // id
protected:
    int id_;
public:
    int getId() const;
    void setId(int id);

    // Robot State
protected:
    RobotState robot_state_;
public:
    RobotState getRobotState() const;
protected:
    void setRobotState(RobotState robot_state);

    // move_base server
    /*
protected:
    actionlib::SimpleActionServer<move_base_msgs::MoveBaseAction>* move_base_action_srv_;
protected:
    void executeCB(const move_base_msgs::MoveBaseGoalConstPtr &goal);
    void goalCB();
    void preemptCB();
    */

    // move_base interface
    // move_base client -> To send a goal to move_base
protected:
    actionlib::SimpleActionClient<move_base_msgs::MoveBaseAction>* move_base_action_cli_;
protected:
    void resultCB(const actionlib::SimpleClientGoalState& state,
                  const move_base_msgs::MoveBaseResultConstPtr& result);
    void activeCb();
    void feedbackCb(const move_base_msgs::MoveBaseFeedbackConstPtr& feedback);

protected:
    nav_msgs::Path planned_trajectory_;
    ros::Subscriber planned_trajectory_sub_;
    void plannedTrajectoryCallback(const nav_msgs::PathPtr& msg);


    // map interface
protected:
    nav_msgs::OccupancyGrid map_;
    ros::Subscriber map_sub_;
    void mapCallback(const nav_msgs::OccupancyGridPtr& msg);

    // localization interface
public:
    int getPose(geometry_msgs::PoseStamped& pose_robot_wrt_world) const;


    // move commands user interface
public:
    int moveToPoint(const geometry_msgs::PoseStamped& pose);
protected:
    ros::Subscriber move_to_point_sub_;
    void moveToPointCallback(const geometry_msgs::PoseStampedPtr& msg);

public:
    int cancelMoveCommand();
    ros::Subscriber cancel_move_cmd_sub_;
    void cancelMoveCommandCallback(const std_msgs::EmptyPtr& msg);


    // mission planner
protected:
    MissionPlanner mission_planner_;

    // explore
public:
    int exploreMap2D(const Eigen::Vector2d& final_point, const Map2DMask& map_mask);
    // service (and action would be better)
protected:
    ros::ServiceServer explore_map2d_srv_;
    bool exploreMap2dSrvCallback(exploration_msgs::ExplorationMission::Request& req, exploration_msgs::ExplorationMission::Response& res);

    // start
public:
    int start();
protected:
    ros::ServiceServer start_srv_;
    bool startSrvCallback(std_srvs::Empty::Request& req, std_srvs::Empty::Response& res);

    // stop
public:
    int stop();
protected:
    ros::ServiceServer stop_srv_;
    bool stopSrvCallback(std_srvs::Empty::Request& req, std_srvs::Empty::Response& res);

    // isStarted
public:
    bool isStarted() const;

protected:
    void init();

protected:
    void create();
    void destroy();

protected:
    void readParameters();

protected:
    void startThreads();
    void stopThreads();

public:
    void open();
    void close();

public:
    void run();
    void auto_run();

protected:
    std::thread* spinner_thread_;
protected:
    void spinnerThread();


};








#endif
