
#ifndef _MISSION_PLANNER_H_
#define _MISSION_PLANNER_H_


// vector
#include <vector>

// list
#include <list>

// iterator
#include <iterator>

// string
#include <string>

// thread
#include <thread>

// random
#include <random>

// limits
#include <limits>

// Eigen
#include <Eigen/Dense>

//Opencv
#include <opencv2/opencv.hpp>

// ros
#include <ros/ros.h>

// map mask 2d
#include "robot_controller/map_mask2d.h"





////////////////////////////////
/// \brief The MissionPlanner class
////////////////////////////////
class MissionPlanner
{
public:
    MissionPlanner();
    ~MissionPlanner();

    // Mission Parameters
protected:
public:
    Eigen::Vector2d init_point_;
    Eigen::Vector2d final_point_;
    Map2DMask map_mask_;

    // mission finished
protected:
    bool mission_finished_;
public:
    bool isMissionFinished() const;

    // mission goals
private:
    std::list<Eigen::Vector2d> mission_goals_;
public:
    int getMissionGoal(Eigen::Vector2d& mission_goal) const;

    // clustering of the map
private:
    int createClustersMap2DByAreaClusters(std::list<Eigen::Vector2d>& mission_goals, const Map2DMask& map_mask, int area_clusters);
    int createClustersMap2DByNumClusters(std::list<Eigen::Vector2d>& mission_goals, const Map2DMask& map_mask, int num_clusters);

    // parameters of clustering the map
private:
    const int area_per_cluster_=36000; // 10 clusters
    //const int area_per_cluster_=20000; // 18 clusters

    // salesman travel problem
private:
    int orderMissionGoals(std::list<Eigen::Vector2d>& mission_goals, const Eigen::Vector2d& init_point, const Eigen::Vector2d& final_point);

    //
private:
    int createExplorationPlan(std::list<Eigen::Vector2d>& mission_goals, const Eigen::Vector2d& init_point, const Eigen::Vector2d& final_point, const Map2DMask& map_mask);

    //
public:
    int createExplorationPlan();

    // Mission Goal Result
protected:
    int num_trials_mission_goal_;
private:
    int achieveMissionGoal();
public:
    int missionGoalAchieved();
    int missionGoalFailed();

    // Parameters of mission goal failed behaviour
private:
    const int num_max_trials_mission_goal_=5;
    const double max_delta_mission_goal_=5;

    //
private:
    std::random_device rd_;
    std::mt19937 gen_;


    // Mission Monitor
public:
    int missionMonitor();

    // start
private:
    bool flag_started_;
public:
    int start();
    int stop();
    bool isStarted() const;


};










#endif
