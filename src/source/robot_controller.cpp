
#include "robot_controller/robot_controller.h"






RobotController::RobotController(int argc,char **argv)
{
    //Ros Init
    ros::init(argc, argv, ros::this_node::getName());

    // Node handle
    nh_=new ros::NodeHandle();

    // Init
    init();

    // Create
    create();

    // Read parameters
    readParameters();

    // start Threads
    startThreads();

    return;
}

RobotController::~RobotController()
{
    // close
    try
    {
        close();
    }
    catch(...)
    {
        // Do nothing
        ROS_INFO("error close()");
    }

    // be tidy: destroy
    try
    {
        destroy();
    }
    catch(...)
    {
        // Do nothing
        ROS_INFO("error destroy()");
    }

    // stop threads
    stopThreads();

    return;
}

void RobotController::init()
{
    // robot state
    robot_state_=RobotState::stopped;

    // frames names
    robot_frame_name_="base_link";
    world_frame_name_="world";

    // id
    id_=-1;

}

void RobotController::create()
{
    // tf listener
    tf_transform_listener_=new tf::TransformListener;
    // tf broadcaster
    //tf_transform_broadcaster_=new tf::TransformBroadcaster;

    return;
}

void RobotController::destroy()
{
    // tf listener
    delete tf_transform_listener_;
    // tf broadcaster
    //delete tf_transform_broadcaster_;

    return;
}

void RobotController::readParameters()
{
    // id
    ros::param::param<int>("~robot_id", id_, 0);
    ROS_INFO("robot_id=%d",id_);
    // robot_frame_name
    ros::param::param<std::string>("~robot_frame_name", robot_frame_name_, "base_link");
    ROS_INFO("robot_frame_name=%s",robot_frame_name_.c_str());
    // world_frame_name
    ros::param::param<std::string>("~world_frame_name", world_frame_name_, "world");
    ROS_INFO("world_frame_name=%s",world_frame_name_.c_str());

    return;
}

void RobotController::startThreads()
{
    spinner_thread_=new std::thread(&RobotController::spinnerThread, this);

    return;
}

void RobotController::stopThreads()
{
    // Do nothing
}

void RobotController::open()
{
    // Start service
    start_srv_ = nh_->advertiseService(ros::this_node::getName()+"/start", &RobotController::startSrvCallback, this);

    // Stop service
    stop_srv_ = nh_->advertiseService(ros::this_node::getName()+"/stop", &RobotController::stopSrvCallback, this);


    // move to point
    move_to_point_sub_ = nh_->subscribe(ros::this_node::getName()+"/move_to_point", 10, &RobotController::moveToPointCallback, this);

    // cancel move to point
    cancel_move_cmd_sub_ = nh_->subscribe(ros::this_node::getName()+"/cancel_move_cmd", 10, &RobotController::cancelMoveCommandCallback, this);


    // Action Server
    //move_base_action_srv_=new actionlib::SimpleActionServer<move_base_msgs::MoveBaseAction>(*nh_, "/move_base",  boost::bind(&RobotController::executeCB, this, _1), false);
    //register the goal and feeback callbacks
    //move_base_action_srv_->registerGoalCallback(boost::bind(&RobotController::goalCB, this));
    //move_base_action_srv_->registerPreemptCallback(boost::bind(&RobotController::preemptCB, this));
    //subscribe to the data topic of interest
    // = nh_->subscribe("/random_number", 1, &AveragingAction::analysisCB, this);
    // start
    //move_base_action_srv_->start();

    // Action Client -> to send goals
    move_base_action_cli_=new actionlib::SimpleActionClient<move_base_msgs::MoveBaseAction>("move_base", true);
    move_base_action_cli_->waitForServer();


    //
    planned_trajectory_sub_ = nh_->subscribe("move_base_node/NavfnROS/plan", 10, &RobotController::plannedTrajectoryCallback, this);

    //
    map_sub_ = nh_->subscribe("map", 10, &RobotController::mapCallback, this);


    // explore map2d service
    explore_map2d_srv_ = nh_->advertiseService(ros::this_node::getName()+"/explore_map2d", &RobotController::exploreMap2dSrvCallback, this);


    return;
}

void RobotController::close()
{
    // cancel goal
    move_base_action_cli_->cancelGoal();
    //
    delete move_base_action_cli_;

    return;
}

void RobotController::run()
{
    // Sleep a while to allow everything to be inited
    // TODO Improve
    ros::Duration(1.0).sleep();

    // Do nothing

    // Waits until the other thread dies
    spinner_thread_->join();

    return;
}

void RobotController::auto_run()
{
    // Sleep a while to allow everything to be inited
    // TODO Improve
    ros::Duration(1.0).sleep();

    // Creates map to explore
    Map2DMask map_mask;
    map_mask.width_=600;
    map_mask.height_=600;
    map_mask.resolution_=0.1;
    map_mask.prepareMask();
    map_mask.initMaskToOnes();

    // Final Point
    Eigen::Vector2d final_point;
    final_point(0)=55;
    final_point(1)=55;

    // start robot
    start();

    // explore map 2d
    exploreMap2D(final_point, map_mask);

    // Waits until the other thread dies
    spinner_thread_->join();

    // stop robot (not really needed)
    stop();

    return;
}

int RobotController::getId() const
{
    return id_;
}

void RobotController::setId(int id)
{
    id_=id;
}

RobotState RobotController::getRobotState() const
{
    return robot_state_;
}

void RobotController::setRobotState(RobotState robot_state)
{
    robot_state_=robot_state;
}

int RobotController::start()
{
    // State Machine to change robot_state
    switch(robot_state_)
    {
        case RobotState::started:
            return 0;
        case RobotState::stopped:
            // change robot state
            setRobotState(RobotState::started);
            // start mission planner
            mission_planner_.start();
            return 0;
        default:
            break;
    }
    return -1;
}

bool RobotController::startSrvCallback(std_srvs::Empty::Request& req, std_srvs::Empty::Response& res)
{
    //
    ROS_INFO("RobotController::startSrvCallback");

    if(start())
        return false;
    return true;
}

bool RobotController::stopSrvCallback(std_srvs::Empty::Request& req, std_srvs::Empty::Response& res)
{
    //
    ROS_INFO("RobotController::stopSrvCallback");

    if(stop())
        return false;
    return true;
}

int RobotController::stop()
{
    // State Machine to change robot_state
    switch(robot_state_)
    {
        case RobotState::started:
            // set robot state
            setRobotState(RobotState::stopped);
            // stop mission planner
            mission_planner_.stop();
            return 0;
        case RobotState::stopped:
            return 0;
        default:
            break;
    }
    return -1;
}

bool RobotController::isStarted() const
{
    if(robot_state_==RobotState::started)
        return true;
    return false;
}

/*
void RobotController::executeCB(const move_base_msgs::MoveBaseGoalConstPtr& goal)
{
    // Do lots of awesome groundbreaking robot stuff here
    move_base_action_srv_->setSucceeded();
}

void RobotController::goalCB()
{
    // reset helper variables
    //data_count_ = 0;
    //sum_ = 0;
    //sum_sq_ = 0;
    // accept the new goal
    //goal_ = move_base_action_srv_->acceptNewGoal()->samples;
}

void RobotController::preemptCB()
{
    //ROS_INFO("%s: Preempted", action_name_.c_str());
    // set the action state to preempted
    move_base_action_srv_->setPreempted();
}
*/



void RobotController::resultCB(const actionlib::SimpleClientGoalState& state,
                                const move_base_msgs::MoveBaseResultConstPtr& result)
{
    ROS_INFO("GOAL RESULT: Finished in state [%s]", state.toString().c_str());

    switch(state.state_)
    {
        case state.ABORTED:
        {
            ROS_ERROR("GOAL RESULT: unable to reach goal");

            // Notify the mission planner for fail
            mission_planner_.missionGoalFailed();

            break;
        }
        case state.SUCCEEDED:
        {

            ROS_INFO("GOAL RESULT: success");

            // Notify the mission planner for success
            mission_planner_.missionGoalAchieved();
            break;
        }
        default:
            ROS_ERROR("GOAL RESULT: other");
    }



    // send new goal
    if(!mission_planner_.isMissionFinished())
    {
        Eigen::Vector2d mission_goal;
        int error_get_mission_goal=mission_planner_.getMissionGoal(mission_goal);
        if(error_get_mission_goal)
        {
            ROS_ERROR("error_get_mission_goal");
            return;
        }
        ROS_INFO("Next Mission Goal: x=%f, y=%f", mission_goal(0), mission_goal(1));
        geometry_msgs::PoseStamped pose;
        pose.header.frame_id=world_frame_name_;
        pose.header.stamp=ros::Time::now();
        pose.pose.position.x=mission_goal(0);
        pose.pose.position.y=mission_goal(1);
        pose.pose.position.z=0;
        pose.pose.orientation.w=1;
        pose.pose.orientation.x=0;
        pose.pose.orientation.y=0;
        pose.pose.orientation.z=0;
        moveToPoint(pose);
    }

}

// Called once when the goal becomes active
void RobotController::activeCb()
{
    ROS_INFO("Goal just went active");
}

// Called every time feedback is received for the goal
void RobotController::feedbackCb(const move_base_msgs::MoveBaseFeedbackConstPtr& feedback)
{
  //ROS_INFO("Got Feedback of length %lu", feedback->sequence.size());
    //ROS_INFO("Got Feedback of x=%f, y=%f", feedback->base_position.pose.position.x, feedback->base_position.pose.position.y);

    geometry_msgs::PoseStamped pose_robot_wrt_world;
    getPose(pose_robot_wrt_world);

    //ROS_INFO("getPose of x=%f, y=%f", pose_robot_wrt_world.pose.position.x, pose_robot_wrt_world.pose.position.y);

    mission_planner_.missionMonitor();

    return;
}

void RobotController::plannedTrajectoryCallback(const nav_msgs::PathPtr& msg)
{
    planned_trajectory_=*msg;

    // check planned trajectory
    if(planned_trajectory_.poses.empty())
    {
        //ROS_ERROR("unable to find trajectory");
    }
    else
    {
        // trajectory size
        double trajectory_size=0;
        for(int i=1; i<planned_trajectory_.poses.size(); i++)
        {
            double segment_size=pow(planned_trajectory_.poses[i].pose.position.x-planned_trajectory_.poses[i-1].pose.position.x, 2)+
                                    pow(planned_trajectory_.poses[i].pose.position.y-planned_trajectory_.poses[i-1].pose.position.y, 2)+
                                    pow(planned_trajectory_.poses[i].pose.position.z-planned_trajectory_.poses[i-1].pose.position.z, 2);
            segment_size=sqrt(segment_size);
            trajectory_size+=segment_size;
        }
        //ROS_INFO("trajectory_size=%f",trajectory_size);
    }

    return;
}

void RobotController::mapCallback(const nav_msgs::OccupancyGridPtr& msg)
{
    map_=*msg;

    return;
}

int RobotController::getPose(geometry_msgs::PoseStamped &pose_robot_wrt_world) const
{
    ros::Time time_stamp=ros::Time(0);
    tf::StampedTransform transform_pose_robot_wrt_world;
    try
    {
        tf_transform_listener_->lookupTransform(world_frame_name_, robot_frame_name_,
                           time_stamp, transform_pose_robot_wrt_world);
    }
    catch(tf::TransformException &ex)
    {
        return -1;
    }
    tf::poseStampedTFToMsg(tf::Stamped<tf::Pose>(transform_pose_robot_wrt_world, time_stamp, world_frame_name_), pose_robot_wrt_world);
    return 0;
}

int RobotController::moveToPoint(const geometry_msgs::PoseStamped& pose)
{
    // check
    if(!isStarted())
        return 1;

    // Send test goal
    // Prepare msg
    move_base_msgs::MoveBaseGoal goal;
    goal.target_pose=pose;

    // Send goal
    move_base_action_cli_->sendGoal(goal,
                                    boost::bind(&RobotController::resultCB, this, _1, _2),
                                    boost::bind(&RobotController::activeCb, this),
                                    boost::bind(&RobotController::feedbackCb, this, _1));

    ROS_INFO("GOAL SENT TO: x=%f, y=%f", pose.pose.position.x, pose.pose.position.y);

    return 0;
}

void RobotController::moveToPointCallback(const geometry_msgs::PoseStampedPtr& msg)
{
    moveToPoint(*msg);
}

int RobotController::cancelMoveCommand()
{
    move_base_action_cli_->cancelGoal();

    return 0;
}

void RobotController::cancelMoveCommandCallback(const std_msgs::EmptyPtr& msg)
{
    cancelMoveCommand();
}

void RobotController::spinnerThread()
{
    ros::spin();
}



int RobotController::exploreMap2D(const Eigen::Vector2d& final_point, const Map2DMask &map_mask)
{
    // Set Mission Parameters
    // set map to mission planner
    mission_planner_.map_mask_=map_mask;
    // set points to mission planner
    // Init point
    Eigen::Vector2d init_point;
    geometry_msgs::PoseStamped pose_robot_wrt_world;
    int error_get_pose=getPose(pose_robot_wrt_world);
    if(error_get_pose)
        ROS_ERROR("error_get_pose");
    init_point(0)=pose_robot_wrt_world.pose.position.x;
    init_point(1)=pose_robot_wrt_world.pose.position.y;
    mission_planner_.init_point_=init_point;
    // Final Point
    mission_planner_.final_point_=final_point;

    // create exploration plan
    mission_planner_.createExplorationPlan();

    // send first mission point
    Eigen::Vector2d mission_goal;
    mission_planner_.getMissionGoal(mission_goal);
    geometry_msgs::PoseStamped pose;
    pose.header.frame_id=world_frame_name_;
    pose.header.stamp=ros::Time::now();
    pose.pose.position.x=mission_goal(0);
    pose.pose.position.y=mission_goal(1);
    pose.pose.position.z=0;
    pose.pose.orientation.w=1;
    pose.pose.orientation.x=0;
    pose.pose.orientation.y=0;
    pose.pose.orientation.z=0;
    moveToPoint(pose);

    return 0;
}

bool RobotController::exploreMap2dSrvCallback(exploration_msgs::ExplorationMission::Request& req, exploration_msgs::ExplorationMission::Response& res)
{
    //
    ROS_INFO("RobotController::exploreMap2dSrvCallback");

    // Request

    // Final Point
    Eigen::Vector2d final_point;
    final_point(0)=req.final_point.pose.position.x;
    final_point(1)=req.final_point.pose.position.y;

    // Map
    Map2DMask map_mask;
    map_mask.resolution_=req.map_mask.info.resolution;
    map_mask.height_=req.map_mask.info.height;
    map_mask.width_=req.map_mask.info.width;
    map_mask.prepareMask();
    int val_i=0;
    for(int i=0; i<map_mask.width_; i++)
    {
        for(int j=0; j<map_mask.height_; j++)
        {
            map_mask.setMaskValue(req.map_mask.data[val_i], i, j);
            val_i++;
        }
    }

    // Exploration command
    exploreMap2D(final_point, map_mask);


    // Response
    res.accepted=true;

    return true;
}
