
#include "robot_controller/mission_planner.h"




MissionPlanner::MissionPlanner() :
    gen_(rd_())
{
    flag_started_=false;
    mission_finished_=false;
    num_trials_mission_goal_=0;
}

MissionPlanner::~MissionPlanner()
{

}

bool MissionPlanner::isMissionFinished() const
{
    return mission_finished_;
}

int MissionPlanner::getMissionGoal(Eigen::Vector2d& mission_goal) const
{
    if(!isStarted())
    {
        ROS_INFO("Mission Planner is not started!");
        return 1;
    }

    if(!mission_goals_.empty())
    {
        mission_goal=mission_goals_.front();
        return 0;
    }
    else
    {
        if(mission_finished_)
        {
            return 1;
        }
        else
        {
            mission_goal=final_point_;
        }
    }
    return 0;
}

int MissionPlanner::createClustersMap2DByAreaClusters(std::list<Eigen::Vector2d>& mission_goals, const Map2DMask& map_mask, int area_clusters)
{
    // check map
    if(!map_mask.isOk())
        return -1;

    // area
    int total_area=map_mask.getValidArea();

    // set the points in a variable
    cv::Mat points_to_explore;
    points_to_explore.create(total_area,2, CV_32F);
    int num_points_explore_i=0;
    for(int i=0; i<map_mask.width_; i++)
    {
        for(int j=0; j<map_mask.height_; j++)
        {
            if(map_mask.getMaskValue(i,j)!=0)
            {
                points_to_explore.at<float>(num_points_explore_i,0)=i;
                points_to_explore.at<float>(num_points_explore_i,1)=j;
                num_points_explore_i++;
            }
        }
    }

    // num clusters
    int num_clusters=floor(total_area/area_clusters);
    ROS_INFO("num_clusters=%d",num_clusters);

    // clustering
    cv::Mat labels;
    int attempts=5;
    cv::Mat mission_goals_map;

    double compactness = 0.0;
    cv::TermCriteria criteria_kmeans(CV_TERMCRIT_EPS+CV_TERMCRIT_ITER, 50, 0.02);

    try
    {
        compactness = cv::kmeans(points_to_explore, num_clusters, labels, criteria_kmeans, attempts, cv::KMEANS_RANDOM_CENTERS, mission_goals_map);
    }
    catch(...)
    {
        ROS_ERROR("error calculating kmeans");
    }

    // Transform mission goals
    mission_goals.resize(0);
    for(int i=0; i<mission_goals_map.rows; i++)
    {
        mission_goals.push_back(Eigen::Vector2d(mission_goals_map.at<float>(i,0)*map_mask.resolution_, mission_goals_map.at<float>(i,1)*map_mask.resolution_));
    }

    return 0;
}

int MissionPlanner::createClustersMap2DByNumClusters(std::list<Eigen::Vector2d>& mission_goals, const Map2DMask& map_mask, int num_clusters)
{

    cv::Mat points_to_explore;
    int num_points_explore=0;

    // check map
    if(!map_mask.isOk())
        return -1;

    // area
    num_points_explore=map_mask.getValidArea();

    // set the points in a variable
    points_to_explore.create(num_points_explore,2, CV_32F);
    int num_points_explore_i=0;
    for(int i=0; i<map_mask.width_; i++)
    {
        for(int j=0; j<map_mask.height_; j++)
        {
            if(map_mask.getMaskValue(i,j)!=0)
            {
                points_to_explore.at<float>(num_points_explore_i,0)=i;
                points_to_explore.at<float>(num_points_explore_i,1)=j;
                num_points_explore_i++;
            }
        }
    }

    // clustering
    cv::Mat labels;
    int attempts=5;
    cv::Mat mission_goals_map;

    double compactness = 0.0;
    cv::TermCriteria criteria_kmeans(CV_TERMCRIT_EPS+CV_TERMCRIT_ITER, 50, 0.02);

    try
    {
        compactness = cv::kmeans(points_to_explore, num_clusters, labels, criteria_kmeans, attempts, cv::KMEANS_RANDOM_CENTERS, mission_goals_map);
    }
    catch(...)
    {
        ROS_ERROR("error calculating kmeans");
    }

    // Transform mission goals
    mission_goals.resize(0);
    for(int i=0; i<mission_goals_map.rows; i++)
    {
        mission_goals.push_back(Eigen::Vector2d(mission_goals_map.at<float>(i,0)*map_mask.resolution_, mission_goals_map.at<float>(i,1)*map_mask.resolution_));
    }


    return 0;
}

int MissionPlanner::orderMissionGoals(std::list<Eigen::Vector2d> &mission_goals, const Eigen::Vector2d& init_point, const Eigen::Vector2d& final_point)
{
    // Find closest to init and fin point
    std::list<Eigen::Vector2d>::iterator near_mission_goal;
    //
    double distance_min;

    // Find closest to the init point
    near_mission_goal=mission_goals.end();
    //
    distance_min=std::numeric_limits<double>::infinity();
    for(std::list<Eigen::Vector2d>::iterator it_mission_goals=mission_goals.begin();
        it_mission_goals!=mission_goals.end();
        ++it_mission_goals)
    {
        Eigen::Vector2d difference=(*it_mission_goals)-init_point;
        double distance=pow(difference(0),2)+pow(difference(1),2);
        if(distance<distance_min)
        {
            distance_min=distance;
            near_mission_goal=it_mission_goals;
        }
    }
    if(near_mission_goal!=mission_goals.end())
    {
        mission_goals.push_front((*near_mission_goal));
        mission_goals.erase(near_mission_goal);
    }

    // Find closest to the final point
    near_mission_goal=mission_goals.end();
    distance_min=std::numeric_limits<double>::infinity();
    for(std::list<Eigen::Vector2d>::iterator it_mission_goals=mission_goals.begin();
        it_mission_goals!=mission_goals.end();
        ++it_mission_goals)
    {
        Eigen::Vector2d difference=(*it_mission_goals)-final_point;
        double distance=pow(difference(0),2)+pow(difference(1),2);
        if(distance<distance_min)
        {
            distance_min=distance;
            near_mission_goal=it_mission_goals;
        }
    }
    if(near_mission_goal!=mission_goals.end())
    {
        mission_goals.push_back((*near_mission_goal));
        mission_goals.erase(near_mission_goal);
    }



    // Order the rest
    std::list<Eigen::Vector2d>::iterator front_mission_goal=mission_goals.begin();
    std::list<Eigen::Vector2d>::iterator back_mission_goal=std::prev(mission_goals.end());

    // display
    /*
    ROS_INFO("Mission goals before order 1:");
    for(std::list<Eigen::Vector2d>::const_iterator it_mission_goals=mission_goals_.begin();
        it_mission_goals!=mission_goals_.end();
        ++it_mission_goals)
    {
        std::cout<<(*it_mission_goals)(0)<<"; "<<(*it_mission_goals)(1)<<std::endl;
    }
    */


    // First ordering: smallest distance
    for(std::list<Eigen::Vector2d>::iterator it_mission_goals=front_mission_goal;
        it_mission_goals!=std::prev(back_mission_goal);
        ++it_mission_goals)
    {
        Eigen::Vector2d p1=(*it_mission_goals);
        //std::cout<<"checking:"<<std::endl;
        //std::cout<<"p1: "<<p1(0)<<"; "<<p1(1)<<std::endl;

        double distance_min=std::numeric_limits<double>::infinity();
        std::list<Eigen::Vector2d>::iterator p2_i=std::prev(back_mission_goal);

        for(std::list<Eigen::Vector2d>::iterator it_mission_goals2=std::next(it_mission_goals);
            it_mission_goals2!=std::prev(back_mission_goal);
            ++it_mission_goals2)
        {
            Eigen::Vector2d p2=(*it_mission_goals2);
            //std::cout<<"p2: "<<p2(0)<<"; "<<p2(1)<<std::endl;

            Eigen::Vector2d diff_p1_p2=p1-p2;
            double distance=sqrt(pow(diff_p1_p2(0),2)+pow(diff_p1_p2(1),2));
            //std::cout<<"distance="<<distance<<std::endl;

            if(distance<distance_min)
            {
                distance_min=distance;
                p2_i=it_mission_goals2;
                //std::cout<<"distance_min="<<distance_min<<std::endl;
            }
        }

        // change the value
        //std::cout<<"p2_final: "<<(*p2_i)(0)<<"; "<<(*p2_i)(1)<<std::endl;
        mission_goals.insert(std::next(it_mission_goals),(*p2_i));
        mission_goals.erase(p2_i);

    }


    // display
    /*
    ROS_INFO("Mission goals before order 2:");
    for(std::list<Eigen::Vector2d>::const_iterator it_mission_goals=mission_goals_.begin();
        it_mission_goals!=mission_goals_.end();
        ++it_mission_goals)
    {
        std::cout<<(*it_mission_goals)(0)<<"; "<<(*it_mission_goals)(1)<<std::endl;
    }
    */


    // Second ordering: p1-23-4 or p1-32-4
    bool flag_ordered=false;
    while(!flag_ordered)
    {
        //
        flag_ordered=true;

        // calculate total distance
        double distance_total=0;
        for(std::list<Eigen::Vector2d>::iterator it_mission_goals=front_mission_goal;
            it_mission_goals!=std::prev(back_mission_goal);
            ++it_mission_goals)
        {
            Eigen::Vector2d difference=(*it_mission_goals)-(*std::next(it_mission_goals));
            distance_total+=sqrt(pow(difference(0),2)+pow(difference(1),2));
        }

        // Check if the permutation of the element i+1 will improve the distance
        for(std::list<Eigen::Vector2d>::iterator it_mission_goals=std::next(front_mission_goal);
            it_mission_goals!=std::prev(std::prev(back_mission_goal));
            ++it_mission_goals)
        {
            /*
            std::cout<<"checking:"<<std::endl;
            std::cout<<"p1: "<<(*std::prev(it_mission_goals))(0)<<"; "<<(*std::prev(it_mission_goals))(1)<<std::endl;
            std::cout<<"p2: "<<(*it_mission_goals)(0)<<"; "<<(*it_mission_goals)(1)<<std::endl;
            std::cout<<"p3: "<<(*std::next(it_mission_goals))(0)<<"; "<<(*std::next(it_mission_goals))(1)<<std::endl;
            std::cout<<"p4: "<<(*std::next(std::next(it_mission_goals)))(0)<<"; "<<(*std::next(std::next(it_mission_goals)))(1)<<std::endl;
            */

            Eigen::Vector2d p1=(*std::prev(it_mission_goals));
            Eigen::Vector2d p2=(*it_mission_goals);
            Eigen::Vector2d p3=(*std::next(it_mission_goals));
            Eigen::Vector2d p4=(*std::next(std::next(it_mission_goals)));

            double distance_original=0;
            Eigen::Vector2d difference_original_12=p1-p2;
            distance_original+=sqrt(pow(difference_original_12(0),2)+pow(difference_original_12(1),2));
            Eigen::Vector2d difference_original_23=p2-p3;
            distance_original+=sqrt(pow(difference_original_23(0),2)+pow(difference_original_23(1),2));
            Eigen::Vector2d difference_original_34=p3-p4;
            distance_original+=sqrt(pow(difference_original_34(0),2)+pow(difference_original_34(1),2));

            double distance_permuted=0;
            Eigen::Vector2d difference_permuted_13=p1-p3;
            distance_permuted+=sqrt(pow(difference_permuted_13(0),2)+pow(difference_permuted_13(1),2));
            Eigen::Vector2d difference_permuted_32=p3-p2;
            distance_permuted+=sqrt(pow(difference_permuted_32(0),2)+pow(difference_permuted_32(1),2));
            Eigen::Vector2d difference_permuted_24=p2-p4;
            distance_permuted+=sqrt(pow(difference_permuted_24(0),2)+pow(difference_permuted_24(1),2));

            if(distance_permuted < distance_original)
            {
                // do the permutation
                //std::cout<<"permuting p2 and p3"<<std::endl;
                mission_goals.erase(std::next(it_mission_goals));
                mission_goals.insert(it_mission_goals, p3);


                // break
                flag_ordered=false;
                break;
            }
        }
    }

    // Third ordering: p1-234-5 or p1-432-5 or p1-423-5 or p1-324-5 or p1-342-5
    // TODO


    // Higher order ordering
    // TODO Explore recursivity


    // TODO Use trajectory planner (move_base -> srv make_plan) to get trajectories and calculate the optimum path


    return 0;
}

int MissionPlanner::createExplorationPlan(std::list<Eigen::Vector2d>& mission_goals, const Eigen::Vector2d& init_point, const Eigen::Vector2d& final_point, const Map2DMask& map_mask)
{
    // create mission points based on clusters
    createClustersMap2DByAreaClusters(mission_goals, map_mask, area_per_cluster_);

    // order the clusters
    orderMissionGoals(mission_goals, init_point, final_point);

    // display
    ROS_INFO("Mission goals ordered:");
    std::cout<<init_point(0)<<" "<<init_point(1)<<std::endl;
    for(std::list<Eigen::Vector2d>::const_iterator it_mission_goals=mission_goals.begin();
        it_mission_goals!=mission_goals.end();
        ++it_mission_goals)
    {
        std::cout<<(*it_mission_goals)(0)<<" "<<(*it_mission_goals)(1)<<std::endl;
    }
    std::cout<<final_point(0)<<" "<<final_point(1)<<std::endl;

    // end
    return 0;
}

int MissionPlanner::createExplorationPlan()
{
    // checks
    // TODO

    //
    createExplorationPlan(mission_goals_, init_point_, final_point_, map_mask_);

    return 0;
}

int MissionPlanner::achieveMissionGoal()
{
    // remove
    if(!mission_goals_.empty())
        mission_goals_.pop_front();
    else
    {
        mission_finished_=true;
    }

    // reset trials
    num_trials_mission_goal_=0;

    return 0;
}

int MissionPlanner::missionGoalAchieved()
{
    //
    if(!isStarted())
        return 1;

    //
    achieveMissionGoal();

    return 0;
}

int MissionPlanner::missionGoalFailed()
{
    //
    if(!isStarted())
        return 1;

    // Try a number of times before giving up
    num_trials_mission_goal_++;
    if(num_trials_mission_goal_ < num_max_trials_mission_goal_)
    {
        // Randomly move a little the mission goal
        std::uniform_real_distribution<> dis(-max_delta_mission_goal_, max_delta_mission_goal_);
        double dx=dis(gen_);
        double dy=dis(gen_);

        mission_goals_.front()+=Eigen::Vector2d(dx,dy);
    }
    else
    {
        // consider achieved
        achieveMissionGoal();
    }

    return 0;
}

int MissionPlanner::missionMonitor()
{
    if(!isStarted())
        return 1;

    // Implement a behaviour to reorder mission goals if the position of the robot is near other goals
    // TODO

    return 0;
}

int MissionPlanner::start()
{
    if(!flag_started_)
        flag_started_=true;
    return 0;
}

int MissionPlanner::stop()
{
    if(flag_started_)
        flag_started_=false;
    return 0;
}

bool MissionPlanner::isStarted() const
{
    return flag_started_;
}

