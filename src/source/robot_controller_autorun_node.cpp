

//I/O stream
//std::cout
#include <iostream>

//
#include "robot_controller/robot_controller.h"


int main(int argc,char **argv)
{
    RobotController robot_controller(argc, argv);
    ROS_INFO("Starting %s",ros::this_node::getName().c_str());
    // open
    robot_controller.open();
    // run
    robot_controller.auto_run();

    return 0;
}

