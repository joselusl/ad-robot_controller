
#include "robot_controller/map_mask2d.h"




bool Map2DMask::isOk() const
{
    if(mask_.cols() == height_ && mask_.rows() == width_)
        return true;
    return false;
}

Eigen::MatrixXi Map2DMask::getMask() const
{
    return mask_;
}

void Map2DMask::prepareMask()
{
    mask_.resize(width_, height_);
    mask_.setZero();
    return;
}

void Map2DMask::initMaskToOnes()
{
    mask_.setOnes();
}

int Map2DMask::getMaskValue(int pos_x, int pos_y) const
{
    return mask_(pos_y, pos_x);
}

void Map2DMask::setMaskValue(int value, int pos_x, int pos_y)
{
    mask_(pos_y, pos_x)=value;
    return;
}

int Map2DMask::getValidArea() const
{
    // check map
    if(!isOk())
        return -1;
    // set the area
    int valid_area=0;
    for(int i=0; i<width_; i++)
    {
        for(int j=0; j<height_; j++)
        {
            if(mask_(i,j)!=0)
            {
                //points_to_explore.push_back(cv::Point2f(i, j));
                valid_area++;
            }
        }
    }
    return valid_area;
}

